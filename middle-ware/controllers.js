const fs = require('fs');
const cwd = process.cwd();

function addMapping(router, mapping) {

    for (var url in mapping) {
        if (url.startsWith('GET ')) {
            // 如果url类似"GET xxx":
            var path = url.substring(4);
            router.get(path, mapping[url]);
            console.log(`register URL mapping: GET ${path}`);
        } else if (url.startsWith('POST ')) {
            // 如果url类似"GET xxx":
            var path = url.substring(5);
            router.post(path, mapping[url]);
            console.log(`register URL mapping: POST ${path}`);
        } else {
            // 无效的URL:
            console.log(`invalid URL: ${url}`);
        }
    }
}

function addController(router, dir) {

    var files = fs.readdirSync(dir);
    var js_files = files.filter((f) => {
        return f.endsWith('.js');
    });

    for (var f of js_files) {
        console.log(`process controller: ${f}...`);
        // 导入js文件:
        let mapping = require(dir + '/' + f);
        addMapping(router, mapping);

    }


}


module.exports = function(dir) {
    let controller_dir = dir || "/controllers",
        router = require('koa-router')();

    addController(router, controller_dir);

    return router.routes();

}