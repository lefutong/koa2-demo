const Koa = require('koa');

const app = new Koa();

const bodyParser = require('koa-bodyparser');

const controller = require(__dirname + '/middle-ware/controllers.js');
app.use(bodyParser());

app.use(controller(__dirname + "/controllers"));

app.listen(3000);
console.log('app started at port 3000...');